import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Pemain } from '../model/pemain';
import { PemainService } from '../service/pemain.service';
import { PostpemainComponent } from 'src/app/pemain/postpemain/postpemain.component';

@Component({
  selector: 'app-pemain',
  templateUrl: './pemain.component.html',
  styleUrls: ['./pemain.component.scss']
})
export class PemainComponent implements OnInit {

  constructor(
    private pemainService: PemainService,
    public dialog: MatDialog) { }


  displayedColumns = ['tim', 'nama', 'tinggiBadan', 'beratBadan', 'posisi', 'noPunggung', 'action']
  dataSource: any;
  pemain: Pemain;

  tablesortParam: any;
  tablesortTitle: any;
  tablepageIndex: any;
  tablepageSize: any;
  numberOfRows: number;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit(): void {
    this.getPagination({
      pageIndex: 0,
      pageSize: 3,
      length: 0
    });
  }

  getPagination(event: PageEvent) {
    this.tablesortTitle = 'id';
    this.tablesortParam = 'DESC';
    this.tablepageIndex = event.pageIndex + 1;
    this.tablepageSize = event.pageSize;
    this.getTable();
  }

  getTable() {
    this.pemainService.getTableData(
      this.tablesortTitle,
      this.tablesortParam,
      this.tablepageIndex,
      this.tablepageSize
    ).subscribe(
      res => {
        console.log(res);
        this.dataSource = res;
        this.numberOfRows = this.dataSource.totalElements;
        this.dataSource = this.dataSource.content;
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PostpemainComponent, {
      width: '45%',
      data: null
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.ngOnInit();
        console.log(res);
      });
  }

  getPemainDetail(id: number, tim: number, nama: string, tinggiBadan: number, beratBadan: number, posisi: string, noPunggung: number) {
    const dialogRef = this.dialog.open(PostpemainComponent, {
      width: '45%',
      data: { id, tim, nama, tinggiBadan, beratBadan, posisi, noPunggung }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.ngOnInit();
        console.log(res);
      });
  }

  deletePemainDetail(id: number) {
    this.pemainService.deleteDataPemain(id)
      .subscribe(res => {
        this.ngOnInit();
        console.log('Data has been deleted', res);
      });
  }

}
