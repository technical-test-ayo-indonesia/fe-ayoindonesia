import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup, NgForm, FormControl } from '@angular/forms';
import { Pemain } from 'src/app/model/pemain';
import { PemainService } from 'src/app/service/pemain.service';

@Component({
  selector: 'app-postpemain',
  templateUrl: './postpemain.component.html',
  styleUrls: ['./postpemain.component.scss']
})
export class PostpemainComponent implements OnInit {
  matcher: any;

  constructor(
    public dialogRef: MatDialogRef<PostpemainComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Pemain,
    private pemainService: PemainService,
    private formBuilder: FormBuilder) { }

  pemainForm = new FormGroup({
    tim: new FormControl(),
    nama: new FormControl(),
    tinggiBadan: new FormControl(),
    beratBadan: new FormControl(),
    posisi: new FormControl(),
    noPunggung: new FormControl()
  });
  ngOnInit(): void {
    this.pemainForm = this.formBuilder.group({
      'tim': [null, Validators.required],
      'nama': [null, Validators.required],
      'tinggiBadan': [null, Validators.required],
      'beratBadan': [null, Validators.required],
      'posisi': [null, Validators.required],
      'noPunggung': [null, Validators.required]
    });
    if (this.data != null) {
      this.getDataPemain();
    }
  }

  onFormSubmit(form: NgForm) {
    this.pemainService.addDataPemain(form)
      .subscribe(res => {
        console.log('onFormSubmit : ', res)
        this.dialogRef.close();
      }, (err) => {
        console.log('onFormSubmit : ', err)
      });
  }

  onFormSubmitUpdate(form: NgForm) {
    this.pemainService.updateDataPemain(this.data.id, form)
      .subscribe(res => {
        console.log('onFormSubmitUpdate : ', res)
        this.dialogRef.close();
      }, (err) => {
        console.log('onFormSubmitUpdate : ', err)
      });
  }

  getDataPemain() {
    this.pemainForm.setValue({
      tim: this.data.tim,
      nama: this.data.nama,
      tinggiBadan: this.data.tinggiBadan,
      beratBadan: this.data.beratBadan,
      posisi: this.data.posisi,
      noPunggung: this.data.noPunggung
    });
  }

}
