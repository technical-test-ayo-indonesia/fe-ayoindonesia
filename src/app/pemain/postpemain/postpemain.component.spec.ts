import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostpemainComponent } from './postpemain.component';

describe('PostpemainComponent', () => {
  let component: PostpemainComponent;
  let fixture: ComponentFixture<PostpemainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostpemainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostpemainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
