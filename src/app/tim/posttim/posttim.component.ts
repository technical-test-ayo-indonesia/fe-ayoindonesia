import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup, NgForm, FormControl } from '@angular/forms';
import { Tim } from 'src/app/model/tim';
import { TimService } from 'src/app/service/tim.service';


@Component({
  selector: 'app-posttim',
  templateUrl: './posttim.component.html',
  styleUrls: ['./posttim.component.scss']
})
export class PosttimComponent implements OnInit {
  matcher: any;

  constructor(
    public dialogRef: MatDialogRef<PosttimComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Tim,
    private timService: TimService,
    private formBuilder: FormBuilder) { }

  timForm = new FormGroup({
    nama: new FormControl(),
    logo: new FormControl(),
    tahun: new FormControl(),
    alamat: new FormControl(),
    kota: new FormControl()
  });

  ngOnInit(): void {
    this.timForm = this.formBuilder.group({
      'nama': [null, Validators.required],
      'logo': [null, Validators.required],
      'tahun': [null, Validators.required],
      'alamat': [null, Validators.required],
      'kota': [null, Validators.required]
    });
    if (this.data != null) {
      this.getDataTim();
    }
  }

  onFormSubmit(form: NgForm) {
    this.timService.addDataTim(form)
      .subscribe(res => {
        console.log('onFormSubmit : ', res)
        this.dialogRef.close();
      }, (err) => {
        console.log('onFormSubmit : ', err)
      });
  }

  onFormSubmitUpdate(form: NgForm) {
    this.timService.updateDataTim(this.data.id, form)
      .subscribe(res => {
        console.log('onFormSubmitUpdate : ', res)
        this.dialogRef.close();
      }, (err) => {
        console.log('onFormSubmitUpdate : ', err)
      });
  }

  getDataTim() {
    this.timForm.setValue({
      nama: this.data.nama,
      logo: this.data.logo,
      tahun: this.data.tahun,
      alamat: this.data.alamat,
      kota: this.data.kota
    });
  }

}
