import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosttimComponent } from './posttim.component';

describe('PosttimComponent', () => {
  let component: PosttimComponent;
  let fixture: ComponentFixture<PosttimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosttimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosttimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
