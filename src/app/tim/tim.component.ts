import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Tim } from '../model/tim';
import { TimService } from '../service/tim.service';
import { MatDialog } from '@angular/material/dialog';
import { PosttimComponent } from '../tim/posttim/posttim.component';

@Component({
  selector: 'app-tim',
  templateUrl: './tim.component.html',
  styleUrls: ['./tim.component.scss']
})
export class TimComponent implements OnInit {

  constructor(
    private timService: TimService,
    public dialog: MatDialog) { }

  displayedColumns = ['nama', 'logo', 'tahun', 'alamat', 'kota', 'action']
  dataSource: any;
  tim: Tim;

  tablesortParam: any;
  tablesortTitle: any;
  tablepageIndex: any;
  tablepageSize: any;
  numberOfRows: number;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit() {
    this.getPagination({
      pageIndex: 0,
      pageSize: 3,
      length: 0
    });
  }

  getPagination(event: PageEvent) {
    this.tablesortTitle = 'id';
    this.tablesortParam = 'DESC';
    this.tablepageIndex = event.pageIndex + 1;
    this.tablepageSize = event.pageSize;
    this.getTable();
  }

  getTable() {
    this.timService.getTableData(
      this.tablesortTitle,
      this.tablesortParam,
      this.tablepageIndex,
      this.tablepageSize
    ).subscribe(
      res => {
        console.log(res);
        this.dataSource = res;
        this.numberOfRows = this.dataSource.totalElements;
        this.dataSource = this.dataSource.content;
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PosttimComponent, {
      width: '45%',
      data: null
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.ngOnInit();
        console.log(res);
      });
  }

  getTimDetail(id: number, nama: string, logo: string, tahun: string, alamat: string, kota: string) {
    const dialogRef = this.dialog.open(PosttimComponent, {
      width: '45%',
      data: { id, nama, logo, tahun, alamat, kota }
    });

    dialogRef.afterClosed()
      .subscribe(res => {
        this.ngOnInit();
        console.log(res);
      });
  }

  deleteTimDetail(id: number) {
    this.timService.deleteDataTim(id)
      .subscribe(res => {
        this.ngOnInit();
        console.log('Data has been deleted', res);
      });
  }
}
