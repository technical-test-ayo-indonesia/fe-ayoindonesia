import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostjadwalComponent } from './postjadwal.component';

describe('PostjadwalComponent', () => {
  let component: PostjadwalComponent;
  let fixture: ComponentFixture<PostjadwalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostjadwalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostjadwalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
