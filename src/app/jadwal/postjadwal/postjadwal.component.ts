import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup, NgForm, FormControl } from '@angular/forms';
import { Jadwal } from 'src/app/model/jadwal';
import { JadwalService } from 'src/app/service/jadwal.service';

@Component({
  selector: 'app-postjadwal',
  templateUrl: './postjadwal.component.html',
  styleUrls: ['./postjadwal.component.scss']
})
export class PostjadwalComponent implements OnInit {

  matcher: any;

  constructor(
    public dialogRef: MatDialogRef<PostjadwalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Jadwal,
    private jadwalService: JadwalService,
    private formBuilder: FormBuilder) { }

  jadwalForm = new FormGroup({
    tanggal: new FormControl(),
    waktu: new FormControl(),
    tuan: new FormControl(),
    tamu: new FormControl()
  });

  ngOnInit(): void {
    this.jadwalForm = this.formBuilder.group({
      'tanggal': [null, Validators.required],
      'waktu': [null, Validators.required],
      'tuan': [null, Validators.required],
      'tamu': [null, Validators.required]
    });
    if (this.data != null) {
      this.getDataJadwal();
    }
  }

  onFormSubmit(form: NgForm) {
    this.jadwalService.addDataJadwal(form)
      .subscribe(res => {
        console.log('onFormSubmit : ', res)
        this.dialogRef.close();
      }, (err) => {
        console.log('onFormSubmit : ', err)
      });
  }

  onFormSubmitUpdate(form: NgForm) {
    this.jadwalService.updateDataJadwal(this.data.id, form)
      .subscribe(res => {
        console.log('onFormSubmitUpdate : ', res)
        this.dialogRef.close();
      }, (err) => {
        console.log('onFormSubmitUpdate : ', err)
      });
  }

  getDataJadwal() {
    this.jadwalForm.setValue({
      tanggal: this.data.tanggal,
      waktu: this.data.waktu,
      tuan: this.data.tuan,
      tamu: this.data.tamu
    });
  }

}
