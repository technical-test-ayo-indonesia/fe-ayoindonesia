import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TimComponent } from './tim/tim.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { NavToolbarComponent } from './nav-toolbar/nav-toolbar.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { PosttimComponent } from './tim/posttim/posttim.component';
import { PemainComponent } from './pemain/pemain.component';
import { PostpemainComponent } from './pemain/postpemain/postpemain.component';
import { JadwalComponent } from './jadwal/jadwal.component';
import { PostjadwalComponent } from './jadwal/postjadwal/postjadwal.component';
import { LaporanComponent } from './laporan/laporan.component';

@NgModule({
  declarations: [
    AppComponent,
    TimComponent,
    SidenavComponent,
    NavToolbarComponent,
    PosttimComponent,
    PemainComponent,
    PostpemainComponent,
    JadwalComponent,
    PostjadwalComponent,
    LaporanComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatListModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
