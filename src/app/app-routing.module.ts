import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimComponent } from './tim/tim.component';
import { PemainComponent } from './pemain/pemain.component';
import { JadwalComponent } from './jadwal/jadwal.component';
import { LaporanComponent } from './laporan/laporan.component';

// untuk malukukan route navigasi
const routes: Routes = [
  { path: 'tim', component: TimComponent },
  { path: 'pemain', component: PemainComponent },
  { path: 'jadwal', component: JadwalComponent },
  { path: 'laporan', component: LaporanComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
