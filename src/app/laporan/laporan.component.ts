import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Laporan } from '../model/laporan';
import { LaporanService } from '../service/laporan.service';
// import { PostjadwalComponent } from '../jadwal/postjadwal/postjadwal.component';

@Component({
  selector: 'app-laporan',
  templateUrl: './laporan.component.html',
  styleUrls: ['./laporan.component.scss']
})
export class LaporanComponent implements OnInit {

  constructor(
    private laporanService: LaporanService,
    public dialog: MatDialog) { }

  displayedColumns = ['tanggal', 'waktu', 'tuan', 'tamu', 'skortuan', 'skortamu', 'nama', 'menitgoal']
  dataSource: any;
  laporan: Laporan;

  tablesortParam: any;
  tablesortTitle: any;
  tablepageIndex: any;
  tablepageSize: any;
  numberOfRows: number;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit(): void {
    this.getPagination({
      pageIndex: 0,
      pageSize: 3,
      length: 0
    });
  }

  getPagination(event: PageEvent) {
    this.tablesortTitle = 'id';
    this.tablesortParam = 'DESC';
    this.tablepageIndex = event.pageIndex + 1;
    this.tablepageSize = event.pageSize;
    this.getTable();
  }

  getTable() {
    this.laporanService.getTableData(
      this.tablesortTitle,
      this.tablesortParam,
      this.tablepageIndex,
      this.tablepageSize
    ).subscribe(
      res => {
        console.log(res);
        this.dataSource = res;
        this.numberOfRows = this.dataSource.totalElements;
        this.dataSource = this.dataSource.content;
      }
    );
  }

  // openDialog(): void {
  //   const dialogRef = this.dialog.open(PostjadwalComponent, {
  //     width: '45%',
  //     data: null
  //   });

  //   dialogRef.afterClosed()
  //     .subscribe(res => {
  //       this.ngOnInit();
  //       console.log(res);
  //     });
  // }

  // getJadwalDetail(id: number, tanggal: string, waktu: string, tuan: number, tamu: number) {
  //   const dialogRef = this.dialog.open(PostjadwalComponent, {
  //     width: '45%',
  //     data: { id, tanggal, waktu, tuan, tamu }
  //   });

  //   dialogRef.afterClosed()
  //     .subscribe(res => {
  //       this.ngOnInit();
  //       console.log(res);
  //     });
  // }

  // deleteJadwalDetail(id: number) {
  //   this.jadwalService.deleteDataJadwal(id)
  //     .subscribe(res => {
  //       this.ngOnInit();
  //       console.log('Data has been deleted', res);
  //     });
  // }

}
