import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pemain } from '../model/pemain';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PemainService {
  constructor(private http: HttpClient) {
  }

  private apiUrl = "http://192.168.1.104:8080/anggota-tim";

  getDataPemain(): Observable<Pemain[]> {
    return this.http.get<Pemain[]>(this.apiUrl);
  }

  getSatuPemain(id: number): Observable<Pemain> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Pemain>(url)
  }

  addDataPemain(pemain: any): Observable<Pemain> {
    return this.http.post<Pemain>(this.apiUrl, pemain, httpOptions);
  }

  updateDataPemain(id: number, pemain: any): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.put(url, pemain, httpOptions);
  }

  deleteDataPemain(id: number): Observable<Pemain> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete<Pemain>(url, httpOptions);
  }

  getTableData(
    sortTitle?: any,
    sortParam?: any,
    pageIndex?: any,
    pageSize?: any
  ) {
    return this.http.get(
      `${this.apiUrl}?col=${sortTitle}&sort=${sortParam}&page=${pageIndex}&limit=${pageSize}`, httpOptions
    );
  }

}
