import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Jadwal } from '../model/jadwal';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class JadwalService {

  constructor(private http: HttpClient) {
  }

  private apiUrl = "http://192.168.1.104:8080/jadwal";

  getDataJadwal(): Observable<Jadwal[]> {
    return this.http.get<Jadwal[]>(this.apiUrl);
  }

  getSatuJadwal(id: number): Observable<Jadwal> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Jadwal>(url)
  }

  addDataJadwal(jadwal: any): Observable<Jadwal> {
    return this.http.post<Jadwal>(this.apiUrl, jadwal, httpOptions);
  }

  updateDataJadwal(id: number, jadwal: any): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.put(url, jadwal, httpOptions);
  }

  deleteDataJadwal(id: number): Observable<Jadwal> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete<Jadwal>(url, httpOptions);
  }

  getTableData(
    sortTitle?: any,
    sortParam?: any,
    pageIndex?: any,
    pageSize?: any
  ) {
    return this.http.get(
      `${this.apiUrl}?col=${sortTitle}&sort=${sortParam}&page=${pageIndex}&limit=${pageSize}`, httpOptions
    );
  }
}
