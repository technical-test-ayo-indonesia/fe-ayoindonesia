import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Laporan } from '../model/laporan';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LaporanService {

  constructor(private http: HttpClient) {
  }

  private apiUrl = "http://192.168.1.104:8080/papan-goal";

  getDataLaporan(): Observable<Laporan[]> {
    return this.http.get<Laporan[]>(this.apiUrl);
  }

  getSatuLaporan(id: number): Observable<Laporan> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Laporan>(url)
  }

  addDataLaporan(laporan: any): Observable<Laporan> {
    return this.http.post<Laporan>(this.apiUrl, laporan, httpOptions);
  }

  updateDataLaporan(id: number, laporan: any): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.put(url, laporan, httpOptions);
  }

  deleteDataLaporan(id: number): Observable<Laporan> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete<Laporan>(url, httpOptions);
  }

  getTableData(
    sortTitle?: any,
    sortParam?: any,
    pageIndex?: any,
    pageSize?: any
  ) {
    return this.http.get(
      `${this.apiUrl}?col=${sortTitle}&sort=${sortParam}&page=${pageIndex}&limit=${pageSize}`, httpOptions
    );
  }

}
