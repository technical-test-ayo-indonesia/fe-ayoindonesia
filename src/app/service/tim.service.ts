import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tim } from '../model/tim';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TimService {
  constructor(private http: HttpClient) {
  }

  private apiUrl = "http://192.168.1.104:8080/tim";

  getDataTim(): Observable<Tim[]> {
    return this.http.get<Tim[]>(this.apiUrl);
  }

  getSatuTim(id: number): Observable<Tim> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Tim>(url)
  }

  addDataTim(tim: any): Observable<Tim> {
    return this.http.post<Tim>(this.apiUrl, tim, httpOptions);
  }

  updateDataTim(id: number, tim: any): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.put(url, tim, httpOptions);
  }

  deleteDataTim(id: number): Observable<Tim> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete<Tim>(url, httpOptions);
  }

  // mulai dari sini editnya
  // getTableData(
  //   sortTitle?: any,
  //   sortParam?: any,
  //   pageIndex?: any,
  //   pageSize?: any
  // ): Observable<Mahasiswa[]> {
  //   return this.http.get<Mahasiswa[]>(
  //     `${this.apiUrl}?col=${sortTitle}&sort=${sortParam}&page=${pageIndex}&limit=${pageSize}`, httpOptions
  //   );
  // }

  getTableData(
    sortTitle?: any,
    sortParam?: any,
    pageIndex?: any,
    pageSize?: any
  ) {
    return this.http.get(
      `${this.apiUrl}?col=${sortTitle}&sort=${sortParam}&page=${pageIndex}&limit=${pageSize}`, httpOptions
    );
  }
  
}
